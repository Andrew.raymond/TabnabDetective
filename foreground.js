//there should only be one instance of "tabs" which will have an array of "tab"s
var Tabs    = {
  tabArray : [],  //This is an array of type "tab" (eg. array = [tab1, tab2, tab3])

  //Returns whether or not we have this tab in the array yet
  includesTab : function(tab) { 
    return this.tabArray.includes(tab);
  },

  //Adds a tab to the array of tabs
  addTab : function(tab) {
    this.tabArray.push(tab);
  },

  getTabFromName : function(tabName){
    for (i = 0; i < this.tabArray.length; i++){
      if (this.tabArray[i].tabName == tabName){ return this.tabArray[i]; }
    }
  },

  getLengthOfArr : function(){
    return (this.tabArray).length;
  }
};

//Each tab will have a name and an image array
var Tab  = { 
  tabName       : '',
  tabId         : -1,
  imgArray      : [],  //New array for all of the images from one tab

  createTab : function(name){
    this.tabName = name;
    this.tabId = Tabs.getLengthOfArr();  

    return this;
  },

  addImage : function(img) {
    this.imgArray.push(img);
  }
};

//Keep track of downloads
var Downloads = {
  downloadIdArr  : [],
  downloadTabName: [],
  resultingImg: new Image(),

  addDownloadId : function(id){
    this.downloadIdArr.push(id);
  },

  addTabName : function(name){
    this.downloadTabName.push(name);
  }
};

//Main function to take screenshots
var screenshot = {	
  content : document.createElement("canvas"), //The canvas to draw the image on 
  data : '',  								  //Will be populated with the captureVisibleTab function

  	//Function to capture the message from the button that you want to take a screenshot
  	init : function() {	  
	  //Take the screenshot with captureVisibleTab

		chrome.tabs.getSelected(null, function(tab) { // null defaults to current window
          screenshot.title = tab.title;
        }); 

	  	chrome.tabs.captureVisibleTab(null, {format : "png"}, function(data) {
	  	//Set the data for screenshot based on the captureVisibleTab
	     screenshot.data = data;
	      
	     //This is where we are going to go to convert it
	     screenshot.saveScreenshot();
	  });
	},

	/*
	This handles a duplicate file download... Should only get here when we have left tabs and come back.
	- 	The main reason for this function is with the issue that Chrome autonumbers duplicate downloads (eg. filename (1).png) and this triggers percent 
		encoding (changing the space before (1) to %20) when using this string as a filename. We cannot run resemble.js with this space or percent encoding.
	- 	Using this function to delete the file with the (1) in it and replace it with a file that has "Dupe" in it allows us to easily use it with resemble.js
	*/
	handleDuplicate : function(file, link){
		var firstFileName 	= (file.substr(0, file.length - 8) + ".png");
		var newFileName 	= (file.substr(0, file.length - 8) + "Dupe.png");

		console.log("firstFile name: "  + firstFileName);
		console.log("newFile name: "  + newFileName);
		
		//To handle, we download a new file with an altered name with "DUPE" in it rather than a (1) so resemble can handle it 
		chrome.downloads.download({
        	url:      link.href,
        	filename: newFileName.substr(newFileName.indexOf("TND"), newFileName.length)
      	}, function(downloadItemId) {
      		screenshot.sleep(1000);
      		console.log("new file id: " + downloadItemId);
      	});
	},

	//Simple sleep function with millisecond input
	sleep : function(ms) {
  		return new Promise(resolve => setTimeout(resolve, ms));
	},

	//This function is used to create a new image out of the resemble.js output... it will draw the image in a context, and then download the image with a "comparison" keyword in the filename
	interpretData : function(data, currTabName){
		console.log(data);

		//This is the image with the differences:
		var diffImage = new Image();
		diffImage.src = data.getImageDataUrl();

		diffImage.onload = function(){
			var canvas    = screenshot.content;
            canvas.height = diffImage.height;
            canvas.width  = diffImage.width;
            var context   = canvas.getContext("2d");
          
            //Draw the image
            context.drawImage(diffImage, 0, 0);

            //Create the link to download from (make it a hyperlink)
            var link      = document.createElement('a');
            link.href     = screenshot.content.toDataURL();
            link.download = "TND/" + currTabName + "comparison_img.png";
			
			chrome.downloads.download({
		        	url:      link.href,
		        	filename: link.download,
		      	}, function (downloadId) {}
		    );
		    var resultsButton = document.getElementById('resultsButton').disabled = false;
		    Downloads.resultingImg = data;

		    x = chrome.notifications.create('alert', {message: "Results are available... Click 'Show Results' to view them.", type: "basic", title: "TabnabDetective", 
			iconUrl: "https://image.flaticon.com/icons/svg/117/117848.svg"}, function(n){
				setTimeout(function(){ chrome.notifications.clear(n); }, 4000);
		});
		}
	},

	//This function takes the stored resultingImage from resemble.js and builds a new HTML page to open in the background. 
	buttonResults : function(){
		var src = Downloads.resultingImg.getImageDataUrl();
		var bString = 
		"<html><body><h1>Percentage Difference: " + Downloads.resultingImg.misMatchPercentage + "</h1>" +
		"<h1>Has the title changed? " +  titleChanged.toString() + "</h1>" +
		"<p><img id='target' src="+src+" height='800'><p></body></html>";
		
		var bS1 = "";
        var bS2 = "";
        if(Downloads.resultingImg.misMatchPercentage > 0.5){
            bS1 = "<html><body><h1>Percentage Difference: " + "<font color = 'red'>"+Downloads.resultingImg.misMatchPercentage +"</font> "+ "</h1>"
        }else{
            bS1 = "<html><body><h1>Percentage Difference: " + "<font color = 'green'>"+Downloads.resultingImg.misMatchPercentage +"</font> "+ "</h1>"
        }
        if(titleChanged == true){
            bS2 = "<h1>Has the title changed? " +  "<font color = 'red'>"+titleChanged.toString() + "</font>"+"</h1>" 
        }else{
            bS2 = "<h1>Has the title changed? " +  "<font color = 'green'>"+titleChanged.toString() + "</font>"+"</h1>" 
        }
        var bString = 
        bS1 + bS2 + "<p><img id='target' src="+src+" height='800'><p></body></html>";

		var w = window.open();
		w.document.body.innerHTML =  bString;
	},

	//This function takes the current tabName and searches through the downloads. If any of the files contain the tabnName (that aren't comparison) then we delete them.
	cleanUpDownloads : function(tabName){
		chrome.downloads.search({limit: 10, orderBy: ['-startTime']}, function(data){
			for (i in data){
				if (data[i].filename.includes(tabName) && !data[i].filename.includes("comparison")){
					try{
						chrome.downloads.removeFile(data[i].id, null);
					}
					catch(e){console.log("Runtime last error")}
				} 
			}
		});
	},

  	//Function to create a canvas onto which the image will be drawn
 	saveScreenshot : function() {
      	var img     = new Image();
      
        //Once the image is finished being loaded, we want to create this canvas and draw the image starting from the top left
        img.onload  = function() {
          
            //Create the canvas based on image dimensions
            var canvas    = screenshot.content;
            canvas.height = img.height;
            canvas.width  = img.width;
            var context   = canvas.getContext("2d");
          
            //Draw the image
            context.drawImage(img, 0, 0);

            //Create the link to download from (make it a hyperlink)
            var link      = document.createElement('a');

        	//Information update
    	  	chrome.tabs.query({currentWindow : true, active : true}, function(tabs){
    	  		//Prints message for first time diagnostic running.
	          	currentTabId = tabs[0].id;
	          	
	          	//if (checkTabDownloaded(currentTabId) == false){
	          	//	alert("This page is currently safe from tabnabbing attacks, as you have not left it yet.");
	          	//}

	          	if (!(Tabs.tabArray.length != 0) || (Tabs.includesTab(Tabs.getTabFromName(currentTabId)))){
		            currentTab = Tab.createTab(currentTabId);         	//Only create a new tab if there isn't one yet
		            Tabs.addTab(currentTab);

	          	//If there is one, then we can just use the current tab we already have
	            } else {  currentTab = Tabs.getTabFromName(currentTabId); }   

	          	currentTab.addImage(img);

	          	//Can't save the image in any other folders other than Dowloads or its subfolders so that will have to do...
	          	link.href     = screenshot.content.toDataURL();
	          	link.download = "TND/" + currentTab.tabName + "_img.png";

	          	//When to remove the duplicate elements (only when we are on the same tab, and there is no chance that this is a return screenshot)
	            if (currentTab.imgArray.length > 1){
	              	console.log("more than 1")
	             	console.log("did of first: " + Downloads.downloadIdArr[0]);
	              	did = Downloads.downloadIdArr[0]

	          		try{ 		chrome.downloads.removeFile(did, null); }
	          		catch(e){	console.log("Runtime error... already removed"); }

	              	currentTab.imgArray.splice(-1);     //removes the last element in img array
	              	Downloads.downloadIdArr.splice(-1);  //removes last element in the downloadId array
	            }

	            //This downloads the file, and as long as it isn't already in the current tab's "downloadIdArr" then we add it.
	          	chrome.downloads.download({
		        	url:      link.href,
		        	filename: link.download,
		      	}, function fname(downloadId) {
		      		if (!(Downloads.downloadIdArr.includes(downloadId))){
		           		Downloads.addDownloadId(downloadId)
		          	}
		        });

	          	//We then get the most recently downloaded file (the one we just donwloaded), and if it contains "Dupe", we know that we have left the tab and come back, so we need to interpret the data with resemble.js
	     		chrome.downloads.search({limit: 1, orderBy: ['-startTime']}, function(data){
            		chrome.tabs.getSelected(null, function(tab) { // null defaults to current window
                      if(screenshot.title != tab.title){
               			titleChanged = true;
                      }
                  	});

            		console.log("data[" + 0 + "] is " + data[0].filename);
            		var compareTo = data[0].filename.substr(0, data[0].filename.length - 8) + ".png"	//We know the original image's name because it is based off of this one we just downloaded
                    if (data[0].filename.includes("Dupe")){
	            		console.log("Orig: " + data[0].filename +" compared to new: " + compareTo);		
      					resemble(data[0].filename).compareTo(compareTo).onComplete(function(data){		//Compare the images using resemble, and then interpret the data, wait for it to load, and the cleanup the downloads
        					screenshot.interpretData(data, currentTab.tabName);
        					screenshot.sleep(50);
        					screenshot.cleanUpDownloads(currentTab.tabName);
        				});
	            	}

	            	//IF the filename has (1), we need to treat it as a duplicate using the handleDuplicate function
	            	if (data[0].filename.includes("(1)") && data[0].filename.includes(currentTab.tabName)){	
	            		x = chrome.notifications.create('alert', {message: "Please wait. Calculating results...", type: "basic", title: "TabnabDetective", 
						iconUrl: "https://image.flaticon.com/icons/svg/117/117848.svg"}, function(n){
							setTimeout(function(){ chrome.notifications.clear(n); }, 2000);
						});

	            		console.log("We have a duplicate with " + data[0].filename + " and did " + data[0].id);
	   					var fn = data[0].filename;

	   					screenshot.handleDuplicate(fn, link);
		            } 

		            //Otherwise, we just continue on, replacing the downloaded file with the new image (every 3 seconds)
		            else {
		            }
	     		});
	      	});


          //Hide the download bar (we are going to be downloading pretty frequently so...)
          chrome.downloads.setShelfEnabled(false)
          
          //Reset screenshot data
          screenshot.data = '';
        };

    img.src = screenshot.data; 

    //Re-enable the download bar
    chrome.downloads.setShelfEnabled(true)

	//Takes photos every 2 seconds you are browsing this page... 
	//Can just run "screenshot.init" on page load in background maybe... then it will loop through these as well. (Isn't working)
	timerFunct();
	}
};

//STARTS RUNNING HERE
var currentTab; 
var timer;
var titleChanged = false;

//This is set when we hit the "run" button... it is looping every 2000ms or 2s to take a screenshot of the page.
function timerFunct(){
	timer = setTimeout(screenshot.init, 2000);
}

//How we stop the timer
function stopTimer(){
	clearTimeout(timer);
}

//When all of the DOM is loaded, we can get our HTML items such as the runButton and resultsButton
document.addEventListener('DOMContentLoaded', function() {
    var runButton 		= document.getElementById('runButton');
    var resultsButton 	= document.getElementById('resultsButton');

    ///When we click the run button, we start the screenshot
    runButton.addEventListener('click', function() { 
    	var bg = chrome.extension.getBackgroundPage();
    	var addedTabs = bg.listOfTabs();
    	var included = false;

    	chrome.tabs.getSelected(null, function(tab){
    		if (addedTabs.includes(tab.id)){
    			included = true;
    		} else {
    			x = chrome.notifications.create('alert', {message: "This tab has been added to the watchlist!", type: "basic", title: "TabnabDetective", 
					iconUrl: "https://image.flaticon.com/icons/svg/117/117848.svg"}, function(n){
					setTimeout(function(){ chrome.notifications.clear(n); }, 2000);
				});
				bg.addToList(tab.id);
    		}
    	});
    	screenshot.init(); 
    });	

    //Keep disabled until we actually have results to show the user.
    resultsButton.disabled = true;   
    resultsButton.addEventListener('click', screenshot.buttonResults);
});