var listOfVisitedTabs = [];

chrome.tabs.onActivated.addListener(function(info){
	var tabId 	 = info.tabId;
	var	windowId = info.windowId;

	if (listOfVisitedTabs.includes(tabId)){
		x = chrome.notifications.create('alert', 
			{message: "You have left and returned to this tab. If you added this to the watchlist, please run a diagnostic again to check the results.", 
			type: "basic", title: "TabnabDetective", iconUrl: "https://image.flaticon.com/icons/svg/117/117848.svg"}, function(n){
				setTimeout(function(){ chrome.notifications.clear(n); }, 4000);
		}); 

	} else {
		x = chrome.notifications.create('alert2', 
			{message: "Run the diagnostic in order to add this tab to the tabnab attacker watchlist.",
			 type: "basic", title: "TabnabDetective", iconUrl: "https://image.flaticon.com/icons/svg/117/117848.svg"}, function(n){
				setTimeout(function(){ chrome.notifications.clear(n); }, 4000);
		});
	}
});

function listOfTabs(){
	return listOfVisitedTabs;
}

function addToList(id){
	listOfVisitedTabs.push(id);
}


