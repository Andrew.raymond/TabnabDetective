# Installing TabnabDetective
1. Clone or download the repo
1. Open Google Chrome and navigate to chrome://extensions
1. Tick "Developer mode" in the top right
1. Click load unpacked extension and navigate to the download folder for TabnabDetective

# Running TabnabDetective
1. Navigate to a tab other than the extensions page (sometimes extensions don't work on the chrome extensions page...even though it is the most logical place to test them?)
1. Click on the Detective icon in the extensions bar
1. Select "Run Diagnostic"
	* Screenshots will be taken every 3 seconds and the most current one will be kept for each tab
1. Once you leave a tab, return to a tab that you've run a diagnostic on, and then run a second one, you will shortly be notified that results are ready for viewing. 
1. The "Show Results" button won't be greyed out anymore, and you can click it to view the diagnostics in a new tab

# Finding files downloaded during runtime
1. First navigate to your Downloads folder
1. There should be a folder titled TND where all of the files will be downloaded, and cleaned up (still working on the cleaning up part).